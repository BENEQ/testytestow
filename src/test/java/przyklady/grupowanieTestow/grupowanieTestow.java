package przyklady.grupowanieTestow;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import przyklady.przyklad1.Przyklad1Test;
import przyklady.przyklad2.PrzedzialABTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        Przyklad1Test.class, PrzedzialABTest.class  //wskazujemy klasy testowe które chcemy uruchomić
})
public class grupowanieTestow {
}
