package przyklady.parametryzacjaTestow;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(Parameterized.class)
public class LiczbaParzystaTest {
    private int daneWejsciowe;
    private boolean oczekiwanaWartosc;
    private LiczbaParzysta liczbaParzysta;

    @Before
    public void inicjalizacjaTestu() {
        System.out.println("Inicjalizacja testu");
        liczbaParzysta = new LiczbaParzysta();
    }

    public LiczbaParzystaTest(int daneWejsciowe, boolean oczekiwanaWartosc) {
        this.daneWejsciowe = daneWejsciowe;
        this.oczekiwanaWartosc = oczekiwanaWartosc;
    }

    @Parameterized.Parameters
    public static Collection evenNumbers() {
        return Arrays.asList(new Object[][]{
                {-4, true},
                {-3, false},
                {-2, true},
                {-1, false},
                {0, true},
                {1, false},
                {2, true},
                {3, false},
                {4, true},
                {5, false}
        });
    }

    @Test
    public void czyLisbaJestParzystaTest() {
        //given
        System.out.println("Test czyLiczbaJestParzysta: dane oczekiwane: " + oczekiwanaWartosc + " dane wejściowe: " + daneWejsciowe);
        //when
        boolean wynikMetody = liczbaParzysta.czyLisbaJestParzysta(daneWejsciowe);
        //then
        assertThat(wynikMetody).isEqualTo(oczekiwanaWartosc);
    }

}