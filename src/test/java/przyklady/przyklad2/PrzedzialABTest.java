package przyklady.przyklad2;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrzedzialABTest {

    @Test
    public void czyZawieraSieWPrzedzialeTest() {
        //given
        PrzedzialAB przedzialAB = new PrzedzialAB(0, 10);
        boolean warunekBrzegowyA, warunekBrzegowyB;
        boolean liczbaZPrzedzialu, liczbaZPozaPrzedzialu;
        //when
        warunekBrzegowyA = przedzialAB.czyZawieraSieWPrzedziale(0);
        warunekBrzegowyB = przedzialAB.czyZawieraSieWPrzedziale(10);
        liczbaZPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(5);
        liczbaZPozaPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(20);
        //then
        assertTrue(warunekBrzegowyA);
        assertTrue(warunekBrzegowyB);
        assertTrue(liczbaZPrzedzialu);
        assertFalse(liczbaZPozaPrzedzialu);
    }

    @Test
    public void czyZawieraSieWPrzedzialeTest2() {
        //given
        PrzedzialAB przedzialAB = new PrzedzialAB(0, 10);
        boolean warunekBrzegowyA, warunekBrzegowyB;
        boolean liczbaZPrzedzialu, liczbaZPozaPrzedzialu;
        //when
        warunekBrzegowyA = przedzialAB.czyZawieraSieWPrzedziale(0);
        warunekBrzegowyB = przedzialAB.czyZawieraSieWPrzedziale(10);
        liczbaZPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(5);
        liczbaZPozaPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(20);
        //then
        assertTrue(warunekBrzegowyA);
        assertTrue(warunekBrzegowyB);
        assertTrue(liczbaZPozaPrzedzialu); //test się wywala i kończy, nie sprawdza pozostałych asercji
        assertFalse(liczbaZPrzedzialu); //tu też się wywali test jak naprawimy poprzednią linię
        assertTrue(liczbaZPrzedzialu);
        assertFalse(liczbaZPozaPrzedzialu);
    }

    @Test
    public void czyZawieraSieWPrzedzialeTest3() {
        //given
        PrzedzialAB przedzialAB = new PrzedzialAB(0, 10);
        boolean warunekBrzegowyA, warunekBrzegowyB;
        boolean liczbaZPrzedzialu, liczbaZPozaPrzedzialu;
        //when
        warunekBrzegowyA = przedzialAB.czyZawieraSieWPrzedziale(0);
        warunekBrzegowyB = przedzialAB.czyZawieraSieWPrzedziale(10);
        liczbaZPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(5);
        liczbaZPozaPrzedzialu = przedzialAB.czyZawieraSieWPrzedziale(20);
        //then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(warunekBrzegowyA).isTrue();
        softly.assertThat(warunekBrzegowyB).isTrue();
        softly.assertThat(liczbaZPozaPrzedzialu).isTrue(); //test się nie powiedzie, ale sprawdza pozostałych asercji
        softly.assertThat(liczbaZPrzedzialu).isFalse(); //tu też się wywali test ale kolejne asercie nadal będą sprawdzane
        softly.assertThat(liczbaZPrzedzialu).isTrue();
        softly.assertThat(liczbaZPozaPrzedzialu).isFalse();
        softly.assertAll();
        /* alternatywą jest użycie JUnit5 i użycie assertAll, ale to tego trzeba javy 1.8*/
    }

    //test sprawdzający czy rzucany jest wyjątek
    @Test(expected = IllegalArgumentException.class)
    public void przedzialABTest() {
        new PrzedzialAB(10, 0);
    }
}