package przyklady.testowanieKolekcji;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;



public class DniRoboczeTest {
    DniRobocze dniRobocze;

    String[] listaDniRoboczych = {"poniedziałek", "wtorek", "środa", "czwartek", "piatek"};
    String[] listaDniWolnych = {"sobota", "niedziela"};

    @Before
    public void inicjalizacja() {
        dniRobocze = new DniRobocze();
    }

    @Test
    public void dzienTygodniaTest() {
        //given
        List<Integer> daneWejsciowe = new ArrayList<Integer>();
        daneWejsciowe.add(1);
        daneWejsciowe.add(2);
        daneWejsciowe.add(5);
        //when
        List<String> wynikMetody = dniRobocze.dzienTygodnia(daneWejsciowe);
        //then
        assertThat(wynikMetody).isSubsetOf(listaDniRoboczych).doesNotContain(listaDniWolnych).hasSize(3);
    }

    @Test
    public void dzienTygodniaTest2() {
        //given
        List<Integer> daneWejsciowe = new ArrayList<Integer>();
        daneWejsciowe.add(1);
        daneWejsciowe.add(2);
        daneWejsciowe.add(3);
        daneWejsciowe.add(4);
        daneWejsciowe.add(5);
        daneWejsciowe.add(4);
        daneWejsciowe.add(5);
        //when
        List<String> wynikMetody = dniRobocze.dzienTygodnia(daneWejsciowe);
        //then
        assertThat(wynikMetody).isSubsetOf(listaDniRoboczych).doesNotContain(listaDniWolnych).hasSize(7);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void dzienTygodniaTest3() {
        //given
        List<Integer> daneWejsciowe = new ArrayList<Integer>();
        daneWejsciowe.add(1);
        daneWejsciowe.add(6);
        daneWejsciowe.add(5);
        //when
        List<String> wynikMetody = dniRobocze.dzienTygodnia(daneWejsciowe);
        //then
        assertThat(wynikMetody).isSubsetOf(listaDniRoboczych).doesNotContain(listaDniWolnych);
    }

    @Test
    public void dzienTygodniaTest4() {
        //given
        List<Integer> daneWejsciowe = new ArrayList<Integer>();
        //when
        List<String> wynikMetody = dniRobocze.dzienTygodnia(daneWejsciowe);
        //then
        assertThat(wynikMetody).isSubsetOf(listaDniRoboczych).doesNotContain(listaDniWolnych).hasSize(0).isNotNull();
    }

    @Test
    public void dzienTygodniaTest5() {
        //given
        List<Integer> daneWejsciowe = new ArrayList<Integer>();
        daneWejsciowe.add(1);
        daneWejsciowe.add(2);
        daneWejsciowe.add(3);
        daneWejsciowe.add(5);
        daneWejsciowe.add(4);
        //when
        List<String> wynikMetody = dniRobocze.dzienTygodnia(daneWejsciowe);
        //then
        assertThat(wynikMetody).containsSequence("poniedziałek", "wtorek", "środa", "piatek", "czwartek").doesNotContain(listaDniWolnych).hasSize(5);
    }
}