package przyklady.przyklad1;

import org.junit.Test;
import przyklady.przyklad1.Przyklad1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class Przyklad1Test {

    @Test
    public void dlugoscNapisuTest() {
        //given
        Przyklad1 przyklad1 = new Przyklad1("napistestowy");
        //when
        int dlugoscSprawdzana = przyklad1.dlugoscNapisu("napisTestowy");
        //then
        int oczekiwanaDlugosc = 12;

        //asercje Junit
        assertEquals(oczekiwanaDlugosc, dlugoscSprawdzana);
        //assercje AssertJ
        assertThat(dlugoscSprawdzana).isEqualTo(oczekiwanaDlugosc);
    }
}