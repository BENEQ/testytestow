package przyklady.testowanieKolekcji;

import java.util.ArrayList;
import java.util.List;

public class DniRobocze {
    public String[] dniTygodnia = {"poniedziałek", "wtorek", "środa", "czwartek", "piatek"};

    public List<String> dzienTygodnia(List<Integer> listaDni) {
        ArrayList<String> wynik = new ArrayList<String>();
        for (Integer dzien : listaDni) {
            wynik.add(dniTygodnia[dzien - 1]);
        }
        return wynik;
    }
}
