package przyklady.przyklad1;

public class Przyklad1 {
    private String napis;

    public Przyklad1(String napis) {
        this.napis = napis;
    }

    public int dlugoscNapisu(String napis) {
        return napis.length();
    }
}
