package przyklady.przyklad2;

public class PrzedzialAB {
    private int a;
    private int b;

    PrzedzialAB(int a, int b) {
        if (a > b)
            throw new IllegalArgumentException("b jest większe od a, wielkość przedziału powinna być określna od a do b");
        this.a = a;
        this.b = b;
    }

    boolean czyZawieraSieWPrzedziale(int x) {
        if (x >= a && x <= b) return true;
        else return false;
    }
}
